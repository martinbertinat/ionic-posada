angular.module('starter.services', [])

.factory('dataFactory', ['$http', function($http) {
  $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
  //$http.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

  //var urlBase = 'http://mantenimiento.posadasigloxix.com.uy/api';
  var urlBase = 'http://192.168.1.91:8080/appPosada/web/app_dev.php/api';
  //var urlBase = 'http://10.42.0.1/appMantenimientoPosada/web/app_dev.php/api';
  var dataFactory = {};

  dataFactory.getNotas = function () {
      return $http.get(urlBase+'/notas');
  };

  dataFactory.addNota = function (nota) {
      return $http.post(urlBase+'/notas/add', {}, {
        params: nota,
      });
  };

  dataFactory.modificarTarea = function (tarea) {
      return $http.put(urlBase+'/tareas/modificar', {}, {
        params: tarea
      });
  };

  dataFactory.archivarNota = function (nota) {
      return $http.get(urlBase+'/notas/archivar/'+nota.id);
  };

  dataFactory.getTareas = function () {
      return $http.get(urlBase+'/tareas');
  };

  dataFactory.getTarea = function (id) {
      return $http.get(urlBase+'/tareas/'+id);
  };

  dataFactory.getPrioridades = function () {
      var prioridades = [
          {value: 1, nombre:'Baja'},
          {value: 2, nombre:'Media'},
          {value: 3, nombre:'Alta'}
      ];
      return prioridades;
  };

  dataFactory.getPersonas = function () {
      return $http.get(urlBase+'/personas');
  };

  return dataFactory;
}])

.factory('Camera', ['$q', function($q) {
  return {
    getPicture: function(options) {
      var q = $q.defer();

      navigator.camera.getPicture(function(result) {
        // Do any magic you need
        q.resolve(result);
      }, function(err) {
        q.reject(err);
      }, options);

      return q.promise;
    }
  }
}])
