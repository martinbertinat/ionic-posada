angular.module('starter.controllers')

.controller('TareaCompletarCtrl', function($scope, $filter, $stateParams, $state, dataFactory, $cordovaToast) {
    var id = $stateParams.id;
    $scope.tarea = {};
    $scope.global = {
        personas: [],
    };

    dataFactory.getTarea(id).then(function(response){
        $scope.tarea = response.data;
    }, function(error) {
        alert(error);
    });

    dataFactory.getPersonas().then(function(response){
        $scope.global.personas = response.data;
    }, function(error) {
        alert(error);
    });

    $scope.completarTarea = function(){
        var selected = $scope.getOptionsSelected($scope.global.personas, "id", "selected");
        if(selected.length == 0 || $scope.tarea.fechaRealizada == null){
            $cordovaToast.show('Debe ingresar una fecha y seleccionar las personas que realizaron la tarea', 'long', 'bottom');
            return false;
        }

        $scope.tarea.personasCompletadas = JSON.stringify(selected);

        $scope.tarea.realizada = 1;
        dataFactory.modificarTarea($scope.tarea).then(function(response){
            //console.log("r"+response);
            $state.transitionTo('tab.tareas');
        }, function(error) {
            //console.log(error);
            alert(error);
        });
    };

    $scope.getOptionsSelected = function(options, valueProperty, selectedProperty){
        var optionsSelected = $filter('filter')(options, function(option) {return option[selectedProperty] == true; });
        return optionsSelected.map(function(group){ return group[valueProperty]; }).join(", ");
    };

});
