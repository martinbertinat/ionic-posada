angular.module('starter.controllers')

.controller('TareaEliminarCtrl', function($scope, $stateParams, $state, dataFactory, $cordovaToast) {
    var id = $stateParams.id;
    $scope.tarea = {};
    $scope.global = {
        personas: [],
    };

    dataFactory.getTarea(id).then(function(response){
        $scope.tarea = response.data;
    }, function(error) {
        alert(error);
    });

    dataFactory.getPersonas().then(function(response){
        $scope.global.personas = response.data;
    }, function(error) {
        alert(error);
    });

    $scope.eliminarTarea = function(){
        if($scope.tarea.personaEliminada == undefined || $scope.tarea.observacionesRealizada == undefined){
            $cordovaToast.show('Debe ingresar la persona responsable y la razón de eliminación', 'long', 'bottom');
            return false;
        }

        $scope.tarea.eliminada = 1;
        dataFactory.modificarTarea($scope.tarea).then(function(response){
            console.log("r"+response);
            $state.transitionTo('tab.tareas');
        }, function(error) {
            console.log(error);
            alert(error);
        });
    };

});
