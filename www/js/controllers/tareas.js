angular.module('starter.controllers', [])

.controller('TareasCtrl', function($scope, dataFactory, $cordovaCamera, $ionicScrollDelegate, $cordovaToast) {
    $scope.global = {
        btnGuardar: 'Guardar',
        btnGuardarEnabled: true,
        showForm: false
    };
    $scope.tarea = {};
    $scope.tareas = [];

    $scope.cargarTareas = function() {
        $cordovaToast.show('Cargando tareas...', 'short', 'bottom');
        $scope.loadTareas();
    };

    $scope.loadTareas = function (){
        dataFactory.getTareas().then(function(response){
            $scope.tareas = response.data;
            console.log($scope.tareas.lenght);
            if($scope.tareas.length == 0){
                $cordovaToast.show('No se encontraron tareas', 'short', 'bottom');
            }
        }, function(error){
            $cordovaToast.show('Hubo un error al cargar las tareas. ¿Tiene conexión a internet?', 'long', 'bottom');
        });
    };

    $scope.$on('$ionicView.enter', function() {
        $scope.loadTareas();
    });

    $scope.showForm = function(){
        $ionicScrollDelegate.scrollTop(true);
        $scope.global.showForm = !$scope.global.showForm;
    };

    $scope.actualizarApp = function(){
        $cordovaToast.show('Buscando Actualización...', 'short', 'bottom');
        var deploy = new Ionic.Deploy();
        deploy.check().then(function(isDeployAvailable) {
          // isDeployAvailable will be true if there is an update
          // and false otherwise
          if(isDeployAvailable){
              $cordovaToast.show('Actualizando...', 'long', 'bottom');
              deploy.update().then(function(deployResult) {
                  $cordovaToast.show('App Actualizada', 'long', 'bottom');
                  // deployResult will be true when successfull and
                  // false otherwise
                }, function(deployUpdateError) {
                    $cordovaToast.show('Error Actualizando', 'short', 'bottom');
                  // fired if we're unable to check for updates or if any
                  // errors have occured.
                }, function(deployProgress) {
                    $cordovaToast.show('Actualizando '+$deployProgress+'%', 'short', 'bottom');
                });
            } else {
                $cordovaToast.show('No hay ninguna actualización', 'short', 'bottom');
            }
        }, function(deployCheckError) {
            $cordovaToast.show('No se pudo buscar la actualización', 'short', 'bottom');
        });
    };


    $scope.tomarFoto = function() {
        var options = {
            quality : 80,
            destinationType : Camera.DestinationType.FILE_URI,
            allowEdit : false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 1600,
            targetHeight: 1200,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };

        $cordovaCamera.getPicture(options).then(function(imageURI) {
            $scope.tarea.foto = imageURI;
            $cordovaCamera.cleanup();
        }, function(err) {
            $cordovaCamera.cleanup();
            alert("Hubo un error:" + err);
        });
    };

    $scope.agregarTarea = function(){
        if(!$scope.tarea.comentario || !$scope.tarea.foto || !$scope.tarea.prioridad){
            return false;
        }
        $scope.global.btnGuardarEnabled = false;
        $scope.global.btnGuardar = "Subiendo...";
        var options = new FileUploadOptions();
        options.fileKey = "foto";
        options.fileName = $scope.tarea.foto.substr($scope.tarea.foto.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";
        options.chunkedMode = false;
        options.headers = {Connection: "close"};

        options.params = {
            prioridad: $scope.tarea.prioridad,
            comentario: $scope.tarea.comentario
        };

        var ft = new FileTransfer();
        ft.upload($scope.tarea.foto, encodeURI("http://mantenimiento.posadasigloxix.com.uy/api/tareas/add"), function(response) {
            if(response.responseCode == 200){
                $scope.global.btnGuardarEnabled = true;
                $scope.global.btnGuardar = "Guardar";
                $scope.tarea = {};
                $scope.$apply();
            }
        }, function(error) {
            alert("Hubo un error: "+error);
            $scope.global.btnGuardarEnabled = true;
            $scope.global.btnGuardar = "Guardar";
            $scope.$apply();
        }, options);
    }
});
