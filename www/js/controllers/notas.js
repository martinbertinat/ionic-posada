angular.module('starter.controllers')

.controller('NotasCtrl', function($scope, dataFactory, $ionicLoading, $ionicModal) {
    $scope.global = {
        btnGuardarEnabled: true
    };
    $scope.nota = {};
    $scope.notas = [];
    $scope.showForm = false;

    $scope.cargarNotas = function() {
        dataFactory.getNotas().then(function(response){
            $scope.notas = response.data;
        }, function(error){
            alert('Hubo un error al cargar las notas: '+error);
        });
    };

    $scope.cargarNotas();

    $scope.agregarNota = function(){
        if($scope.nota.comentario){
            $scope.global.btnGuardarEnabled = false;
            dataFactory.addNota($scope.nota).then(function(response) {
                $scope.nota = {};
                $scope.global.btnGuardarEnabled = true;
                $scope.cargarNotas();
            }, function(error){
                $scope.global.btnGuardarEnabled = true;
                alert('No se pudo agregar la nota. Intente luego.');
            });
        }
    };

    $scope.archivarNota = function(nota) {
        dataFactory.archivarNota(nota).then(function(response){
            var index = $scope.notas.indexOf(nota);
            $scope.notas.splice(index, 1);
        }, function(error){
            alert("No se pudo archivar la nota. Intente luego.");
        });
    };

    // Check Ionic Deploy for new code
    /*var deploy = new Ionic.Deploy();
    deploy.watch().then(function() {}, function() {}, function(deployUpdateAvailable) {
        if(deployUpdateAvailable == true){
            deploy.update().then(function(res) {
                console.log('Ionic Deploy: Update Success! ', res);
            }, function(err) {
                console.log('Ionic Deploy: Update error! ', err);
            }, function(prog) {
                console.log('Ionic Deploy: Progress... ', prog);
            });
        }
    });*/

});
