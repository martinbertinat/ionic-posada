angular.module('starter.controllers')

.controller('TareaModificarCtrl', function($scope, $stateParams, $state, dataFactory, $filter) {
    var id = $stateParams.id;
    $scope.tarea = {};
    $scope.global = {
        personas: [],
    };

    $scope.global.prioridades = dataFactory.getPrioridades();

    dataFactory.getTarea(id).then(function(response){
        $scope.tarea = response.data;
        dataFactory.getPersonas().then(function(response){
            $scope.global.personas = response.data;
            // selecciono las personas asignadas
            angular.forEach($scope.tarea.personasAsignadas, function(value, key) {
                angular.forEach($scope.global.personas, function(val, ky) {
                    if(val.id == value.id){
                        val.selected = true;
                    }
                });
            });
        }, function(error) {
            alert(error);
        });
    }, function(error) {
        alert(error);
    });



    $scope.modificarTarea = function(){
        var selected = $scope.getOptionsSelected($scope.global.personas, "id", "selected");
        $scope.tarea.personasAsignadas = JSON.stringify(selected);

        console.log($scope.tarea);
        dataFactory.modificarTarea($scope.tarea).then(function(response){
            $state.transitionTo('tab.tareas');
        }, function(error) {
            alert(error);
        });
    };

    $scope.getOptionsSelected = function(options, valueProperty, selectedProperty){
        var optionsSelected = $filter('filter')(options, function(option) {return option[selectedProperty] == true; });
        return optionsSelected.map(function(group){ return group[valueProperty]; }).join(", ");
    };

});
